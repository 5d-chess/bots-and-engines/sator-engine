# Sator Engine

High performance 5D Chess bot designed to interface with 5d-chess-server (https://gitlab.com/alexbay218/5d-chess-server). Designed for clustered systems and high throughput of board evaluation.

## Design

Roles:
 - Interface - One per cluster

   - Handles communication to 5d-chess-server
   - Starts and stops sessions
   - Determines timing strategy
   - Send requests to the Processor
  
 - Processor - One per cluster

   - Grabs initial request and starts tree gen
   - Checks Database for tree nodes and send to Evaluator for processing
   - Build evaluation tree and run alpha-beta

 - Evaluator - Scalable

   - Run board evaluation algorithm
   - Write to database

 - Database - Scalable (Redis)

   - Contain key/value store of board evaluation values

## Copyright

All source code is released under AGPL v3.0 (license can be found under the LICENSE file).

Any addition copyrightable material not covered under AGPL v3.0 is released under CC BY-SA v3.0.
