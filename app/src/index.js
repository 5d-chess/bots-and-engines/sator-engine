const os = require('os');
const { program } = require('commander');
program.version(process.env.npm_package_version);

program.option('-r, --role <role>', 'specify role within cluster [interface, processor, or evaluator]. Defaults to interface.');
program.option('-t, --threads <threads>', 'define number of threads to use (defaults to number of cpu cores)');

program.parse(process.argv);

var opts = program.opts();
var params = {
  role: 'interface',
  threads: os.cpus().length
};

if(typeof opts.role === 'string') { params.role = opts.role; }
if(typeof opts.threads === 'string') { params.threads = Number(opts.threads); }
