const { spawn, Thread, Worker } = require('threads');
const Chess = require('5d-chess-js');
const boardEval = require('./boardEval');

const testTime = 10000;
const maxThreads = 4;

//Single thread model
var tmpChess = new Chess();
var hashRes = [];
var evalRes = [];
var buffer = [tmpChess.state()];
var start = Date.now();
var maxAction = 0;
while(Date.now() < start + testTime) {
  tmpChess.state(buffer[0]);
  var currEval = 0; //boardEval(tmpChess.state());
  hashRes.push(tmpChess.hash);
  evalRes.push(currEval);
  buffer.splice(0,1);
  if(tmpChess.submittable(true)) {
    if(maxAction < tmpChess.rawAction) {
      maxAction = tmpChess.rawAction;
    }
    tmpChess.submit(true);
  }
  var moves = tmpChess.moves('raw', true, true, true);
  for(var i = 0;i < moves.length;i++) {
    tmpChess.move(moves[i]);
    buffer.push(tmpChess.state());
    tmpChess.undo();
  }
}
var end = (Date.now() - start);
console.log('Single Threaded board evals: ' + evalRes.length);
console.log('Single Threaded board evals per second: ' + (evalRes.length / (end / 1000)));
console.log('Took ' + (end / 1000) + ' seconds');
console.log('Single Threaded max action: ' + maxAction);

//Multi-threaded model
tmpChess.reset();
var workers = [];
hashRes = [];
evalRes = [];
buffer = [tmpChess.state()];
maxAction = 0;

var init = async () => {
  for(var i = 0;i < maxThreads;i++) {
    var worker = await spawn(new Worker('./workerEval'));
    workers.push(worker);
  }
};

var run = async () => {
  await init();
  var tasks = [];
  for(var i = 0;i < workers.length;i++) {
    tasks.push(workers[i](tmpChess.state(), start + testTime, i, workers.length));
  }
  var data = await Promise.all(tasks);
  buffer = [];
  for(var j = 0;j < data.length;j++) {
    var currData = data[j];
    for(var i = 0;i < currData.hashes.length;i++) {
      hashRes.push(currData.hashes[i]);
      evalRes.push(currData.evals[i]);
    }
    if(maxAction < currData.action) {
      maxAction = currData.action;
    }
  }
  for(var i = 0;i < workers.length;i++) {
    await Thread.terminate(workers[i]);
  }
  end = (Date.now() - start);
  console.log('Multi-Threaded board evals: ' + evalRes.length);
  console.log('Multi-Threaded board evals per second: ' + (evalRes.length / (end / 1000)));
  console.log('Took ' + (end / 1000) + ' seconds');
  console.log('Multi-Threaded max action: ' + maxAction);
}

start = Date.now();
run();
