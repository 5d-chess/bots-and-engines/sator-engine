const Chess = require('5d-chess-js');
const { expose } = require('threads/worker');
const boardEval = require('./boardEval');
var tmpChess = new Chess();

expose((initState, maxTime, index = 0, maxWorkers = 1) => {
  var buffer = [initState];
  var firstSplit = false;
  var res = {
    hashes: [],
    evals: [],
    action: 0
  };

  //tmpChess.state(initState);
  //var currAction = tmpChess.rawAction;
  while(Date.now() < maxTime) {
    tmpChess.state(buffer[0]);
    var currEval = 0; //boardEval(tmpChess.state());
    res.hashes.push(tmpChess.hash);
    res.evals.push(currEval);
    buffer.splice(0,1);
    if(tmpChess.submittable(true)) {
      if(res.action < tmpChess.rawAction) {
        res.action = tmpChess.rawAction;
      }
      tmpChess.submit(true);
    }
    var moves = tmpChess.moves('raw', true, true, true);
    if(!firstSplit) {
      for(var i = index;i < moves.length;i+=maxWorkers) {
        tmpChess.move(moves[i]);
        buffer.push(tmpChess.state());
        tmpChess.undo();
      }
      firstSplit = true;
    }
    else {
      for(var i = 0;i < moves.length;i++) {
        tmpChess.move(moves[i]);
        buffer.push(tmpChess.state());
        tmpChess.undo();
      }
    }
  }

  return res;
});